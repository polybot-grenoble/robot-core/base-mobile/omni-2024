#include <unit.h>
#include <coordinates.cpp>
#include <vector>

//Coef matthieu pour une base mobile à 3 roues
// const float coef_M1_x = cos(PI/2); //0
// const float coef_M2_x = cos(PI/2 + 2*PI/3);
// const float coef_M3_x = cos(PI/2 + 4*PI/3);

// const float coef_M1_y = sin(PI/2);
// const float coef_M2_y = sin(PI/2 + 2*PI/3);
// const float coef_M3_y = sin(PI/2 + 4*PI/3);

// const float coef_M1_rot = 1;
// const float coef_M2_rot = 1;
// const float coef_M3_rot = 1;

// const float InfluenceTot_x = abs(coef_M1_x) + abs(coef_M2_x) + abs(coef_M3_x);
// const float InfluenceTot_y = abs(coef_M1_y) + abs(coef_M2_y) + abs(coef_M3_y);
// const float InfluenceTot_rot = abs(coef_M1_rot) + abs(coef_M2_rot) + abs(coef_M3_rot);

/**
 * @brief Position local d'un moteur par rapport au centre de la base mobile
 * @protected local_position Une 
 * @public .MotorPosition()
*/
class MotorPosition{
    protected:
    Position local_position;
    public:
    set_Motor_position(mm _x, mm _y, rad _angle){
        this->local_position = new Position((float)_x, (float)_y, (float)_angle);
    }
};





class Odometry{
    protected:
    Position position;
    int nb_motors;
    MotorPosition **motors;

    public:
    Odometry(){
        
    }
}