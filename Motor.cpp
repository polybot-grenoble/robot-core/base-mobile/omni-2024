#include <Arduino.h>
#include <unit.h>
#include <stdint.h>
#include <Pid.cpp>

//Set the printing for debug values
#define debug 1

class Motor{

    private:
    
    pin pin_encoderA,
        pin_encoderB,
        pin_pwm,
        pin_direction_positive,
        pin_direction_negative;

    protected:

    // Géométrie
    mm wheel_perimetre = 182.2;
    unsigned int enc_step_per_revolution = 300;
    
    // Instantané
    int nb_step_encodeur;
    
    // Dernière mesure
    micro_second time_previous;
    mm travelled_distance_last_period;
    mm_per_s speed_last_period;
    rot_per_s rotation_last_period;

    //filtrage de la vitesse;
    rot_per_s angular_speed_filter_now;
    rot_per_s angular_speed_filter_previous;
    rot_per_s angular_speed_raw_now;
    rot_per_s angular_speed_raw_previous;

    //Speed control
    bool do_pid;
    Pid pid_motor_speed;
    uint8_t power;
    uint8_t dir;
    
    public:
    
    Motor(mm wheel_radius, int enc_step_per_revolution,pin pin_encoderA, pin pin_encoderB,
            pin pin_pwm, pin pin_direction_positive, pin pin_direction_negative){

        this->wheel_perimetre = wheel_radius*TWO_PI;
        this->enc_step_per_revolution = enc_step_per_revolution;
        this->pin_encoderA = pin_encoderA;
        this->pin_encoderB = pin_encoderB;
        this->pin_pwm = pin_pwm;
        this->pin_direction_positive = pin_direction_positive;
        this->pin_direction_negative = pin_direction_negative;
        pinMode(pin_encoderA, INPUT);
        pinMode(pin_encoderB, INPUT);
        pinMode(pin_pwm, OUTPUT);
        pinMode(pin_direction_positive, OUTPUT);
        pinMode(pin_direction_negative, OUTPUT);

        attachInterrupt( pin_encoderA,[&](){this->handle_enc_interrupt();}, RISING);

        nb_step_encodeur = 0;
        time_previous = micros();
        do_pid = false;
    }

    //The 1 and -1 choice is related to the omni-base rotation (sens trig = +)
    /**
     * @param dir : (-1) sens trigonométrique, (1) sens anti-trigonométrique, (0) stop
     * @param power : < 255 power maximum
     * @brief Motor control thought power management
    */
    void set_motor_power(uint8_t dir, uint8_t power){
        analogWrite(this->pin_pwm,power);

        digitalWrite(this->pin_direction_positive, dir == 1 ? HIGH : LOW);
        digitalWrite(this->pin_direction_negative, dir == -1 ? HIGH : LOW);

        if(debug){Serial.print(power);Serial.print(" ");Serial.print(dir == 1 ? HIGH : LOW);
Serial.print(" ");Serial.println(dir == -1 ? HIGH : LOW);}

    }

    Motor &update(){
        //Dans la variable nb_step_encodeur_now il y a le nombre de pas d'encodeur depuis la dernière update
        int nb_step_encodeur_now = this->nb_step_encodeur;
        this->nb_step_encodeur -= nb_step_encodeur_now;
        
        micro_second time_now = micros();
        micro_second time_delta = ((time_now - time_previous)/(1e-6));
        
        //Calcul distance
        this->travelled_distance_last_period = nb_step_encodeur_now * wheel_perimetre / enc_step_per_revolution;   

        //Calcul vitesse
        float encodeur_per_s = nb_step_encodeur_now / time_delta;
        rad_per_s angular_speed_raw_now = (encodeur_per_s * 60.0)  / enc_step_per_revolution;

        // la vitesse calculé est sensible au hautes fréquences
        // le filtrage numérique coupe les fréquences >25 Hz
        // v_filtre[now] = 0.854*v_filtre[previous] + 0.0728*v[now] + 0.0728*v[previous]
        this->angular_speed_filter_now = 0.854*angular_speed_filter_previous + 0.0728*angular_speed_raw_now + 0.0728*angular_speed_raw_previous;
        this->angular_speed_raw_previous = angular_speed_raw_now;
        this->angular_speed_filter_previous = angular_speed_filter_now;
        
        if(do_pid){
            pid_motor_speed.execute_pid_control(angular_speed_filter_now,time_delta,this->power,this->dir);
            set_motor_power(this->dir,this->power);
        }

        this->time_previous = time_now;

        return this;
    }
    
    /**
     * @brief Incrémente la distance local dans le sens trigonométrique
    */
    void handle_enc_interrupt(){
        int b = digitalRead(this->pin_encoderB);
        if(b > 0){
            this->nb_step_encodeur++;
        }
        else{
            this->nb_step_encodeur--;
        }
    }

    mm get_travelled_distance(){
        return this->travelled_distance_last_period;
    }

    mm_per_s get_speed(){
        return this->speed_last_period;
    }

    void set_motor_angular_speed(rpm angular_speed_desired){
        this->pid_motor_speed.set_motor_angular_speed(angular_speed_desired);
        this->do_pid == true;
    }

    
};
