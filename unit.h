#ifndef UNIT_H
#define UNIT_H

#include <Arduino.h>

typedef float mm;
typedef float mm_per_s;
typedef float mm_per_s_per_s;

typedef float rad;
typedef float rad_per_s;
typedef float rad_per_s_per_s;

typedef float rpm;

typedef float rot_per_s;

typedef int pin;

typedef unsigned long micro_second;

template<typename type>
type min_max(type min, type x, type max) {
    return min > x ? min : (max < x ? max : x);
}

// rad frame_angle(rad angle) {
//     while (angle < PI)
//         angle += TWO_PI;
//     while (angle > PI)
//         angle -= TWO_PI;
//     return angle;
// }

#endif
