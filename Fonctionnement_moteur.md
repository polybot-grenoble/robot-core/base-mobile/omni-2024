# Moteur Pololu
[Site source ici](https://www.pololu.com/product/4751)

## Sens de rotation
Le sens de rotation trigonometrique actionne l'encodeur B avant l'encodeur A.

Inversement pour le sens horaire.

## Nombre de pas d'encodeur par révolution
18.75:1, 1200 activation d'encodeur par révolution.

## Branchement
|Couleur|fonction|
|---|---|
|Rouge 	|motor power (+)|
|Noir| 	puissance moteur (-)|
|Vert| 	encoder GND|
|Bleu |	encoder Vcc (3.5 – 20 V)|
|Jaune| 	encoder A sortie|
|Blanc| 	encoder B sortie|

## Chronogramme d'encodeur dans le sens trigonométrique
3V 0.9A
+ Rouge (Potentiel moins)
+ Noir (Potentiel plus)
+ Vert Encodeur GND
+ Bleu Encodeur +5V
+ Jaune Encodeur A
+ Blanc Encodeur B

![Chronograme encodeur](images_md/chronogramme_encodeur_moteur_pololu_senstrigo_3V.jpg)

> Courbe jaune : encodeur A <br>
> Courbe bleu : encodeur B

## Cablage
Sur les driver moteur L298N :
### Entrée
STM32 | Driver moteur |
|---|---|
|5V | 5V|
| PIN_drive_positive | IN1|
| PIN_drive_negative | IN2|
| PIN_PWM            | ENA|

|Alimentation|Driver moteur|
|---|---|
| Potentiel -        | GND|
| Potentiel + (12V)  | VMS|

### Sortie
La plus à droite c'est un + <br>
la seconde c'est un -

![Image associé]()
