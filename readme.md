# Utilisation de la base mobile holonome

Code Arduino pour la base mobile holonome à 3 roues omnidirectionnelles, pour la carte NUCLEO L432KC.

## Commands

| id | action | parameters | response | Progress |
|----|--------|------------|----------|----------|
|0|Unplug motors| none | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |
|1|Stop, aim null speed| none | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |
|2|Set speed relative to **robot's origin**| x', y' and θ' (floats) | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |
|3|Set speed relative to **table's origin**| x', y' and θ' (floats) | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |
|4|Go to coordinate relative to **robot's origin**| x, y and θ (floats) | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |
|5|Go to coordinate relative to **table's origin**| x, y and θ (floats) | none | <ul><li>[ ] In Progress</li><li>[ ] Implemented</li></ul> |


## Wiring
Refer to [this file](Fonctionnement_moteur.md)

## Function implementation
Refer to [this file](organisation_du_code.md)

