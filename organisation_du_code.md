# Organisation du code

Présentation de la direction et de l'agencement des éléments présent dans le code du module base omni-directionnel.

## Code structure

```mermaid
classDiagram
    Talos <--> State_machine
    State_machine <--> Odometry
    Odometry --> Motor
    PID <--> Motor

    Motors --> Motor
    Motors <-- Odometry
class Talos{
        
    }
namespace Omni {

    class Odometry{
        #Motor[]
        _motor_influence_by_axes[]
        _position

        set_position()
        get_position()
        update()
    }

    class State_machine{
            
    }

    
    class PID{

    }

    class Motor{
        _encodeur_step_per_revolution
        _wheel_diameter
        _travelled_distance
        _pins
        
        
        set_speed(param speed)
        get_travel_distance(bool reset)
    }

    class Motors{
        wrappeur des updates (enc,distance,speed)
    }


}
style Motors fill:#bbf,stroke:#f66,stroke-width:2px,color:#fff,stroke-dasharray: 5 5
        

```
en bleu, une idée de wrappeur pour avoir tout les calculs d'encodeur, de distance et de vitesse en parallele.

## Development roadmap
|Functions|test|
|---|---|
|- [x] Motor init and set raw power.|✔️|
|- [x] PID to follow a desired motor speed.| |
|- [ ] Odometry with Matthieu's coefficients.| |
|- [ ] Unplug motors command.| |
|- [ ] Zero speed command.| |
|- [ ] Core integration with unplug and zero speed commands.| |
|- [ ] Set relative speed command addition.| |
|- [ ] Set absolute speed command addition.| |
|- [ ] Go to commands addition.| |

## State_machine
Machine à état pour réalisé les différents asservissements de la base mobile.

## Odometry

## Motor

## PID

