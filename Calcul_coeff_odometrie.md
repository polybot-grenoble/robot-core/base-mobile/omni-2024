# Comment calculer l'odometrie local de la base mobile ?
Dans un premier temps, il est utile de réalisé un schéma, un plan ou une définition géometrique de la base mobile. Comme sur la figure 1 suivante :
| ![space-1.jpg](/images_md/GEOGEBRA_BASEMOBILE.png) | 
|:--:| 
| *Figure 1 : Représentation géométrique de la base mobile sur Géogebra* |

Puis pour chaque degré de liberté, X,Y et la rotation en Z. Itérer sur l'impact de tous les moteurs en local. <br>


