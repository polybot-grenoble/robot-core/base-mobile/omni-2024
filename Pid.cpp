#include <Arduino.h>
#include <unit.h>
class Pid{
    private:
        float constant_p, constant_d, constant_i, power_max;
        float error_previous, error_integral;
    
    protected:
        rot_per_s angular_speed_desired;
    
    public:
    Pid() : constant_p(5), constant_d(0.1), constant_i(10), power_max(255), error_previous(0.0), error_integral(0.0){}

    /**
     * @brief Setup of the paramaters for the Pid
     * @param 
    */
    void set_parametters(float Kp, float Kd, float Ki){
        constant_p = Kp;
        constant_d = Kd;
        constant_i = Ki;
        return;
    }

    /**
     * @note À finir et pensez à segmenter les updates du moteur.
    */
    void set_motor_angular_speed(rot_per_s angular_speed_desired){
        this->angular_speed_desired = angular_speed_desired;
    }

    void execute_pid_control(float angular_speed_now, float time_delta, int &power, int &dir){
    // Calcul de l'erreur entre la vitesse desiré et la vitesse actuelle
    float error_now = angular_speed_desired - angular_speed_now;
  
    // derivative
    float dedt = (error_now-error_previous)/(time_delta);
  
    // integral
    this->error_integral = error_integral + error_now*time_delta;

    //anti windup

    if(error_integral > power_max/constant_i)
      error_integral = power_max/constant_i;
    if(error_integral < -power_max/constant_i)
      error_integral = -power_max/constant_i;
  
    // control signal
    float control_signal = constant_p*error_now + constant_d*dedt + constant_i*error_integral;
  
    // motor power
    power = (int) fabs(control_signal);
    if( power > power_max ){
      power = power_max;
    }
  
    // motor direction
    dir = 1;
    if(control_signal<0){
      dir = -1;
    }
  
    // store previous error
    error_previous = error_now;
    }

}
;
